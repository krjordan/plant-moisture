import RPi.GPIO as GPIO
import time

# How often to check the soil moisture when it's dry (the water is on)
dry_poll = 1 # seconds

# How often to check the soil moisture when it's wet ( the water is off)
wet_poll = 15 * 60 # seconds

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN)
GPIO.setup(18, GPIO.OUT)

water = 0

try:
    while True:
        GPIO.output(18, GPIO.HIGH)
        time.sleep(.1) # Wait for the digital sensor to settle
        if GPIO.input(17):
            print ','.join((time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), "Dry"))
            if not water:
                print ','.join((time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), "Turn on the water!"))
                water = 1
        else:
            print ','.join((time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), "Wet"))
            if water:
                print ','.join((time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), "Turn off the water!"))
                water = 0
        GPIO.output(18, GPIO.LOW)
        if water:
            time.sleep(dry_poll)
        else:
            time.sleep(wet_poll)
except KeyboardInterrupt:
    GPIO.cleanup()
